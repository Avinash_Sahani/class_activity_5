package com.example.class_activity_5;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    ListView pslteams;

String teams_names[]={"Karachi Kings","Lahore Qalandars","Quetta Galaditors","Peshawar Zalmi","Multan Sultans"};
int teams_imgs[]={R.drawable.karachikings,R.drawable.lahoreqalandars,R.drawable.quettagaladitors,R.drawable.peshawarzalmi,R.drawable.multansultan};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
pslteams=findViewById(R.id.psl_teams);
        MyAdapter teams_adapter = new MyAdapter(this, teams_names,  teams_imgs);
        pslteams.setAdapter(teams_adapter);

    }
    class MyAdapter extends ArrayAdapter<String>
    {
Context context;
String teams[];
int teams_img[];
        public MyAdapter(@NonNull Context context, String teams[],int teams_img[]) {
            super(context, R.layout.team_item,R.id.team_name,teams);
            this.context=context;
            this.teams=teams;
            this.teams_img=teams_img;

        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.team_item, parent, false);
            ImageView team_img = row.findViewById(R.id.team_image);
            TextView team_name = row.findViewById(R.id.team_name);

            team_img.setImageResource(teams_img[position]);
            team_name.setText(teams[position]);


return row;
        }
    }
}